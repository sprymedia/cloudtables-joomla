<?php
// no direct access
defined( '_JEXEC' ) or die;

jimport( 'joomla.plugin.plugin' );
include_once( JPATH_PLUGINS . '/editors-xtd/cloudtableseditor/Api.php');

class plgContentCloudTablesContent extends JPlugin
{
    protected $app;
    protected $db;

    function plgContentCloudTablesContent(&$subject, $config)
    {
        parent::__construct($subject, $config);
    }

	/**
	 * Plugin method with the same name as the event will be called automatically.
	 */
	 function onContentPrepare($context, &$row, &$params, $page = 0)
	 {
		if ($context !== 'com_content.article') {
			return true;
		}

		if (is_object($row)) {
			$text = &$row->text;
		}
		else {
			$text = &$row;
		}

	    $plugin = JPluginHelper::getPlugin('content', 'cloudtablescontent');

	    if (! $plugin) {
			// Not enabled - do nothing
	        return false;
	    }
 
		$user = JFactory::getUser();
	    $pluginParams = new JRegistry($plugin->params);
	    $apiKey = $pluginParams->get('apikey');
	    $apiKeyEditor = $pluginParams->get('apikey_editor');
	    $subdomain = $pluginParams->get('subdomain');
	    $host = $pluginParams->get('host');

		$cnt = preg_match_all('/\{cloudtable(.*?)\}/', $text, $matches);

		for ($i=0 ; $i<$cnt ; $i++) {
			$matched = $matches[0][$i];
			$tags = $matches[1][$i];

			// Use DOMDocument to parse the tags
			$xml = DOMDocument::loadHTML('<ct '.$tags.'/>');
			$ct = $xml->getElementsByTagName('ct')->item(0);
 
			if ($ct->hasAttributes()) {
				$id = '';
				$key = $user->id
					? $apiKeyEditor
					: $apiKey;

				// Find our attributes
				foreach ($ct->attributes as $attr) {
					if ($attr->nodeName === 'id') {
						$id = $attr->nodeValue;
					}
					else if ($attr->nodeName === 'key') {
						$key = $attr->nodeValue;
					}
				}

				if ($id) {
					if ($host) {
						$api = new CloudTables\Api($key, [
							'domain' => $host,
							'secure' => false
						]);
					}
					else {
						$api = new CloudTables\Api($subdomain, $key);
					}

					$script = $api->scriptTag($id);

					$text = str_replace($matched, $script, $text);
				}
			}
		}


		// TODO
		// User detection - admin or not?
		// Custom api key option in short tag

		return true;
	}
}
