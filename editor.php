<?php

defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');
include_once('Api.php');

use Joomla\Utilities\ArrayHelper;

class PlgButtonCloudTablesEditor extends JPlugin
{
    protected $app;
    protected $db;
    
	public function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
	}

	function onDisplay($editorName, $asset, $author)
	{
	    $app = $this->app; // JFactory::getApplication();

	    // Admin editing only
	    if (!$app->isAdmin()) {
	        return false;
	    }

 	    $extension = $app->input->get('option');

 	    // Editing only
 	    if ($extension != 'com_content') {
            return false;     
 	    }

	    $plugin = JPluginHelper::getPlugin('content', 'cloudtablescontent');
	    if (! $plugin) {
	        $app->enqueueMessage('CloudTables for Joomla! is not enabled. Please go to "Extensions -> Plugins" enable the CloudTables plugin and fill in the access details for it.', 'Warning');
	        return false;
	    }
 
	    $pluginParams = new JRegistry($plugin->params);
	    $apiKey = $pluginParams->get('apikey');
	    $apiKeyEditor = $pluginParams->get('apikey_editor');
	    $subdomain = $pluginParams->get('subdomain');
	    $host = $pluginParams->get('host');

		if (! $apiKey) {
	        $app->enqueueMessage('CloudTables API access key not specified.', 'Warning');
	        $error = true;
		}

		if (! $apiKeyEditor) {
	        $app->enqueueMessage('CloudTables API admin access key not specified.', 'Warning');
	        $error = true;
		}

		if (! $subdomain && !$host) {
	        $app->enqueueMessage('CloudTables sub-domain or host not specified.', 'Warning');
	        $error = true;
		}

		if ($error) {
			$app->enqueueMessage('To configure CloudTables for Joomla!, please go to <a href="/administrator/index.php?option=com_plugins&view=plugins">the plugins page</a> select the <em>CloudTables Content</em> plugin and edit the settings.', 'Warning');
			return false;
		}

		if ($host) {
			$api = new CloudTables\Api($apiKeyEditor, [
				'domain' => $host,
				'secure' => false
			]);
		}
		else {
			$api = new CloudTables\Api($subdomain, $apiKeyEditor);
		}

		$datasets = $api->datasets();
		$json = json_encode($datasets);

		$js = <<<EOD
function cloudTablesBtn(editor) {
	window.event.preventDefault();

	var $ = jQuery;
	var datasets = ${json};
	var host = $('.icon-cloudTablesButton').parent();
	var offset = host.offset();

	// Already showing - bail out
	if ($('div.cloudTables-select').length) {
		return;
	}

	var select = $('<div class="cloudTables-select"></div>')
		.css({
			top: offset.top + host.height() + 10,
			left: offset.left
		})
		.insertAfter('body');

	$.each(datasets, function (i, ds) {
		$('<div class="cloudTables-option"></div>')
			.text(ds.name)
			.on('click', function () {
				jInsertEditorText('{cloudtable id="'+ds.id+'" description="'+ds.name+'"}', editor);
			})
			.appendTo(select);
	});

	setTimeout(function () {
		$(document).on('click.ctBtn', function () {
			$('div.cloudTables-select').remove();
			$(document).off('click.ctBtn');
		});
	}, 10);
}
EOD;

		$css = <<<EOD
.icon-cloudTablesButton {
	background: transparent url(/plugins/editors-xtd/cloudtableseditor/icon.png) no-repeat !important;
}

div.cloudTables-select {
	position: absolute;
	width: 160px;
	max-height: 400px;
	background-color: white;
	z-index: 10;
	padding: 5px 0;
	border: 1px solid rgba(0,0,0,0.2);
	overflow: auto;

	font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
	font-size: 14px;
}

div.cloudTables-option {
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
	padding: 6px 15px 6px 12px;
	margin-bottom: 1px;
	cursor: pointer;
}

div.cloudTables-option:hover {
	background-color: #2d8ac7;
	color: white;
}
EOD;

		$doc = &JFactory::getDocument();
		$doc->addScriptDeclaration($js);
		$doc->addStyleDeclaration($css);

		$button = new JObject();
		$button->set('modal', false);
		$button->set('onclick', 'cloudTablesBtn(\''.$editorName.'\'); return false;');
		$button->set('text', 'CloudTables');
		$button->set('name', 'cloudTablesButton');
		$button->set('link', '#');
		$button->set('class', 'btn');
		return $button;
	}
}
