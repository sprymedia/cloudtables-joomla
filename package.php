<?php
/**
 * @package xbFootnoteTag-Package 
 * @filesource pkg_xbfootnotetag_script.php  
 * @version 1.0.0.0 15th July 2019
 * @desc install, upgrade and uninstall actions
 * @author Roger C-O
 * @copyright (C) Roger Creagh-Osborne, 2019
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/
// No direct access to this file
defined('_JEXEC') or die;

class pkg_cloudTablesPkgInstallerScript
{
    function preflight($type, $parent)
    {   
    }
    
    function install($installer)
    {
        // enable plugins
        $plugin = JTable::getInstance('extension');

        $plugins = array(
            'content' => 'cloudtablescontent',
            'editors-xtd' => 'cloudtableseditor',
        );

        $parent = $installer->getParent();

        foreach ($plugins as $folder => $element) {
            $id = $plugin->find([
                'type' => 'plugin',
                'folder' => $folder,
                'element' => $element
            ]);

            if ($id) {
                $plugin->load($id);
                $plugin->enabled = 1;
                $plugin->store();
            }
        }

        $message = <<<EOD
<h3>CloudTables plug-in installed</h3>

<p>
    Don't forget to set required options for the CloudTables Content and Configuration plugin:
    <a href="index.php?option=com_plugins&filter_search=cloudtables">Goto Plugin Options Pages</a>
</p>

EOD;
        $parent->set('message', $message);

        return true;
    }
    
    function uninstall($parent)
    {
        echo '<p>CloudTables Package has been uninstalled</p>';
    }
    
    function update($parent)
    {
        
    }
    
    function postflight($type, $parent)
    {
        $message = $parent->get('manifest')->name;

        switch ($type) {
            case 'install': $message .= ' version '.$parent->get('manifest')->version.' has been installed'; break;
            case 'uninstall': $message .= ' has been uninstalled'; break;
            case 'update': $message .= ' has been updated to version '.$parent->get('manifest')->version; break;
            case 'discover_install': $message .= 'discovered and installed version '.$parent->get('manifest')->version; break;
        }

        JFactory::getApplication()->enqueueMessage($message);
    }
}
