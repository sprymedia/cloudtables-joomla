#!/bin/sh

if [ -d build ]; then
	rm -rf build
fi

if [ -e cloudtables-joomla.zip ]; then
	rm cloudtables-joomla.zip
fi

if [ -e editor.zip ]; then
	rm editor.zip
fi

if [ -e content.zip ]; then
	rm content.zip
fi


## Button plugin
mkdir build

cp editor.php build/cloudtableseditor.php
cp editor.xml build/cloudtableseditor.xml
cp ../php/Api.php build
cp icon.png build

cd build
zip editor.zip cloudtableseditor.php cloudtableseditor.xml Api.php icon.png
mv editor.zip ..
cd ..

rm -rf build


## Content plugin
mkdir build

cp content.php build/cloudtablescontent.php
cp content.xml build/cloudtablescontent.xml

cd build
zip content.zip cloudtablescontent.php cloudtablescontent.xml
mv content.zip ..
cd ..

rm -rf build


## Package plugin
mkdir build

mv content.zip build
mv editor.zip build
cp package.xml build/pkg_cloudtablespkg.xml
cp package.php build/pkg_cloudtables.php

cd build
zip cloudtables-joomla.zip pkg_cloudtablespkg.xml pkg_cloudtables.php content.zip editor.zip
mv cloudtables-joomla.zip ..
cd ..

rm -rf build



